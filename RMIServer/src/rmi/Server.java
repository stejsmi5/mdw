package rmi;

import java.net.URL;
import java.net.URLClassLoader;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Server extends UnicastRemoteObject implements ConvertInterface{

    public Server() throws RemoteException{
        super();
    }

    public double convert(String from, String to, int amount) throws RemoteException {
        return (double) amount * getCourse(from.toUpperCase(), to.toUpperCase());
    }

    private double getCourse(String from, String to){
        switch (from) {
            case "USD":
                if (to.equals("EUR")) {
                    return 0.9;
                } else if (to.equals("GBP")) {
                    return 0.8;
                }
                break;
            case "EUR":
                if (to.equals("USD")) {
                    return 1.08;
                } else if (to.equals("GBP")) {
                    return 0.88;
                }
                break;
            case "GBP":
                if (to.equals("EUR")) {
                    return 1.12;
                } else if (to.equals("USD")) {
                    return 1.22;
                }
                break;
        }

        return 1;
    }


    public static void main(String[] args) {

        ClassLoader cl = ClassLoader.getSystemClassLoader();

        URL[] urls = ((URLClassLoader)cl).getURLs();

        for(URL url: urls){
            System.out.println(url.getFile());
        }

        try {
            ConvertInterface convert = new Server();
            Naming.rebind("convert", convert);
            System.err.println("rmi.Server ready");
        } catch (Exception e) {
            System.err.println("rmi.Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
