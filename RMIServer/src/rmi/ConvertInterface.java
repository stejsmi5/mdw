package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ConvertInterface extends Remote{
    public double convert(String from, String to, int amount) throws RemoteException;
}
