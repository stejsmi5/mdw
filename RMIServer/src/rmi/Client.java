package rmi;

import java.rmi.Naming;

public class Client {
    private Client() {}

    public static void main(String[] args) {
        
        try {
            ConvertInterface convert = (ConvertInterface) Naming.lookup("rmi://127.0.0.1/convert");
            System.out.print(convert.convert("EUR", "GBP", 11));
        } catch (Exception e) {
            System.err.println("rmi.Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
