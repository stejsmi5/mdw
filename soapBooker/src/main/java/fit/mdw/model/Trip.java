package fit.mdw.model;


public class Trip {
    private String tripName;
    private boolean occupied;
    private String person;

    public Trip(){}

    public Trip(String tripName, boolean occupied, String person) {
        this.tripName = tripName;
        this.occupied = occupied;
        this.person = person;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }
}
