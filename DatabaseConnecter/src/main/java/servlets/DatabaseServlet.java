package main.java.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import weblogic.jdbc.jta.DataSource;
import weblogic.servlet.annotation.WLServlet;

@WLServlet(name = "db", mapping = { "/db" })
public class DatabaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Context ctx = null;
		Hashtable ht = new Hashtable();
		ht.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
		ht.put(Context.PROVIDER_URL, "t3://127.0.0.1:7001");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			
			ctx = new InitialContext(ht);
			javax.sql.DataSource ds = (javax.sql.DataSource) ctx.lookup("database");
			conn = ds.getConnection();
			
			stmt = conn.createStatement();
			stmt.execute("select * from records");
			rs = stmt.getResultSet();
			
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<body>");
			out.println("<h1>Database </h1>");

			while (rs.next()) {
				out.println(rs.getInt("id"));
				out.println(rs.getString("type"));
				out.println(rs.getString("location"));
				out.println(rs.getInt("capacity"));
				out.println(rs.getInt("occupied"));
				out.println(rs.getInt("trip"));
				out.println(rs.getString("person"));
				out.println("<br>");
			}

			out.println("</body>");
			out.println("</html>");

			stmt.close();
			stmt = null;
			conn.close();
			conn = null;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ctx.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
