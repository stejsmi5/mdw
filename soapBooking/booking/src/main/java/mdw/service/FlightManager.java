package mdw.service;


import mdw.model.Flight;
import mdw.model.Trip;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use= SOAPBinding.Use.LITERAL)
public interface FlightManager {

    @WebMethod
    Flight bookFlight(String name, String destination);

    @WebMethod
    List<Flight> getAllFlights();
}