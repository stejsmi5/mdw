
package mdw.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "bookTripResponse", namespace = "http://service.mdw/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bookTripResponse", namespace = "http://service.mdw/")
public class BookTripResponse {

    @XmlElement(name = "return", namespace = "")
    private mdw.model.Trip _return;

    /**
     * 
     * @return
     *     returns Trip
     */
    public mdw.model.Trip getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(mdw.model.Trip _return) {
        this._return = _return;
    }

}
