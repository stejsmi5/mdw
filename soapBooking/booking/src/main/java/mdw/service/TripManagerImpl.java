package mdw.service;


import mdw.model.Trip;
import mdw.repository.TripRepository;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "mdw.service.TripManager")
public class TripManagerImpl implements TripManager{

    @Override
    public Trip bookTrip(String name, String destination) {
        return TripRepository.book(name, destination);
    }

    @Override
    public List<Trip> getAllTrips() {
        return TripRepository.getAll();
    }

}
