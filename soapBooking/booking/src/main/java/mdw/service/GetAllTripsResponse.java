
package mdw.service.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getAllTripsResponse", namespace = "http://service.mdw/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAllTripsResponse", namespace = "http://service.mdw/")
public class GetAllTripsResponse {

    @XmlElement(name = "return", namespace = "")
    private List<mdw.model.Trip> _return;

    /**
     * 
     * @return
     *     returns List<Trip>
     */
    public List<mdw.model.Trip> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<mdw.model.Trip> _return) {
        this._return = _return;
    }

}
