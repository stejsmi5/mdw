package mdw.service;


import mdw.model.Flight;
import mdw.model.Trip;
import mdw.repository.FlightRepository;
import mdw.repository.TripRepository;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "mdw.service.BookManager")
public class BookManagerImpl implements BookManager {

    @Override
    public Object book(String type, String name, String destination) {
        if(type.equals("trip")){
            return TripRepository.book(name, destination);
        }else{
            return FlightRepository.book(name, destination);
        }
    }
}
