package mdw.service;


import mdw.model.Flight;
import mdw.model.Trip;
import mdw.repository.FlightRepository;
import mdw.repository.TripRepository;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "mdw.service.FlightManager")
public class FlightManagerImpl implements FlightManager{


    @Override
    public Flight bookFlight(String name, String destination) {
        return FlightRepository.book(name, destination);
    }

    @Override
    public List<Flight> getAllFlights() {
        return FlightRepository.getAll();
    }
}
