
package mdw.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getAllTrips", namespace = "http://service.mdw/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAllTrips", namespace = "http://service.mdw/")
public class GetAllTrips {


}
