package mdw;

import mdw.service.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.ws.Endpoint;

@SpringBootApplication
public class BookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookingApplication.class, args);
        Endpoint.publish("http://localhost:9999/ws/book", new BookManagerImpl());
		Endpoint.publish("http://localhost:9999/ws/trip", new TripManagerImpl());
		Endpoint.publish("http://localhost:9999/ws/flight", new FlightManagerImpl());
	}
}
