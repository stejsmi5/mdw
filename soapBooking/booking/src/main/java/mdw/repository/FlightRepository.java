package mdw.repository;

import mdw.model.Flight;

import java.util.ArrayList;
import java.util.List;

public class FlightRepository {
    private static FlightRepository ourInstance = new FlightRepository();
    private static List<Flight> flightList;

    public static FlightRepository getInstance() {
        return ourInstance;
    }

    private FlightRepository() {
        flightList = new ArrayList<>();
        init();
    }


    public static Flight book(String name, String destination){
        if(flightList == null){
            flightList = new ArrayList<>();
            init();
        }

        for(int i=0; i<flightList.size(); i++){
            if(destination.equals(flightList.get(i).getName())){
                flightList.get(i).addPerson(name);
                return flightList.get(i);
            }
        }
        return null;
    }



    public static List<Flight> getAll(){
        return flightList;
    }

    private static void init(){
        Flight t1 = new Flight("usa");
        Flight t2 = new Flight("uk");
        Flight t3 = new Flight("ru");

        flightList.add(t1);
        flightList.add(t2);
        flightList.add(t3);
    }
}
