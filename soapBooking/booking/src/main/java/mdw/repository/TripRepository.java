package mdw.repository;

import mdw.model.Trip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TripRepository {

    private static TripRepository ourInstance = new TripRepository();
    private static List<Trip> tripList;

    public static TripRepository getInstance() {
        return ourInstance;
    }

    private TripRepository() {
        tripList = new ArrayList<>();
        init();
    }

    public static Trip book(String name, String destination){
        if(tripList == null){
            tripList = new ArrayList<>();
            init();
        }

        for(int i=0; i<tripList.size(); i++){
            if(destination.equals(tripList.get(i).getName())){
                tripList.get(i).addPerson(name);
                return tripList.get(i);
            }
        }
        return null;
    }

    public static List<Trip> getAll(){
        return tripList;
    }

    private static void init(){
        Trip t1 = new Trip("usa");
        Trip t2 = new Trip("uk");
        Trip t3 = new Trip("ru");

        tripList.add(t1);
        tripList.add(t2);
        tripList.add(t3);
    }
}
