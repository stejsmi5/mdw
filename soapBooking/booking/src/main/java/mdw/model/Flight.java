package mdw.model;

import java.util.ArrayList;
import java.util.List;

public class Flight {
    private String name;
    private List<String> person;

    public Flight(){
        this.person = new ArrayList<>();
    }

    public Flight(String name) {
        this.name = name;
        this.person = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPerson() {
        return person;
    }

    public void setPerson(List<String> person) {
        this.person = person;
    }

    public void addPerson(String name){
        this.person.add(name);
    }
}
