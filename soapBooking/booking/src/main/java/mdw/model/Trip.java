package mdw.model;

import java.util.ArrayList;
import java.util.List;

public class Trip {
    private String name;
    private List<String> person;

    public Trip() {
        this.person = new ArrayList<>();
    }

    public Trip(String name) {
        this.name = name;
        this.person = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPerson() {
        return person;
    }

    public void setPerson(List<String> person) {
        this.person = person;
    }

    public void addPerson(String person){
        this.person.add(person);
    }
}
