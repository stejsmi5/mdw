package fit.ctu.controllers;

import fit.ctu.BalancerApplication;
import fit.ctu.model.ServiceConnection;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@RestController
public class TripController {

    private final static Logger logger = Logger.getLogger(BalancerApplication.class);

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<?> getTrip(){
        String serviceUrl = getRunningService();

        if(serviceUrl != null){
            String response = serviceUrl + "\n";
            try {
                HttpURLConnection connection = (HttpURLConnection) (new URL(serviceUrl)).openConnection();
                connection.setRequestMethod("GET");
                BufferedReader inputStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));


                String inputLine;
                while ((inputLine = inputStream.readLine()) != null) {
                    response += (inputLine.getBytes());
                }

                return new ResponseEntity<>(response, HttpStatus.OK);
            } catch (IOException e) {
                logger.error("no service aviable" + e.getMessage());
            }

        }

        return new ResponseEntity<>("no service aviable", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private String getRunningService(){

        for(ServiceConnection service : BalancerApplication.servicePool){
            if(service.isStatus()){
                return service.getUrl();
            }
        }
        return null;
    }
}
