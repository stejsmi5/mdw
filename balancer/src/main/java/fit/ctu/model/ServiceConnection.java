package fit.ctu.model;


public class ServiceConnection {
    private String url;
    private boolean status;

    public ServiceConnection(String url, boolean status) {
        this.url = url;
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
