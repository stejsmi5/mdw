package fit.ctu;


import fit.ctu.model.ServiceConnection;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class HealthStatus implements Job{

    private final static Logger logger = Logger.getLogger(HealthStatus.class);

    private void checkPoll(List<ServiceConnection> urlList){
        urlList.forEach(this::checkConnection);
    }

    private void checkConnection(ServiceConnection serviceConnection){
        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(serviceConnection.getUrl())).openConnection();
            connection.setRequestMethod("GET");
            int code = connection.getResponseCode();

            serviceConnection.setStatus(code == 200);

            logger.info(serviceConnection.getUrl() + " response with http code " + code );

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        checkPoll(BalancerApplication.servicePool);
    }
}
