import java.util.Hashtable;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Broker implements MessageListener {
    private QueueConnectionFactory qconFactory;

    private QueueConnection connection;
    private QueueSession session;
    private QueueReceiver receiver;
    private Queue queue;

    private QueueConnection connection1;
    private QueueSession session1;
    private QueueSender sender;

    private TextMessage msg;

    private void initSend(Context ctx, String queueName) throws NamingException, JMSException {
        qconFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        connection1 = qconFactory.createQueueConnection();

        session1 = connection1.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue queue = (Queue) ctx.lookup(queueName);

        sender = session1.createSender(queue);
        msg = session1.createTextMessage();
    }

    private void initReceive(Context ctx, String queueName) throws NamingException, JMSException {
        qconFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        connection = qconFactory.createQueueConnection();

        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        queue = (Queue) ctx.lookup(queueName);

        receiver = session.createReceiver(queue);
        receiver.setMessageListener(this);
        connection.start();
    }

    private void close() throws JMSException {
        sender.close();
        session1.close();
        connection1.close();
    }

    private void closeReceive() throws JMSException {
        receiver.close();
        session.close();
        connection.close();
    }

    public void onMessage(Message msg) {
        try {
            String msgText;
            if (msg instanceof TextMessage) {
                msgText = ((TextMessage) msg).getText();
            } else {
                msgText = msg.toString();
            }

            if (msgText.startsWith("book ")) {
                send("jms/Queue", msgText);
            } else if (msgText.startsWith("vylet ")) {
                send("jms/Queue3", msgText);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void send(String queueName, String message) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ic = new InitialContext(env);
        initSend(ic, queueName);
        try {
            msg.setText(message);
            sender.send(msg, DeliveryMode.PERSISTENT, 8, 0);
        } finally {
            close();
        }
    }

    private void receive(String queueName) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ic = new InitialContext(env);
        initReceive(ic, queueName);

        System.out.println(queue.toString());

        try {
            synchronized (this) {
                while (true) {
                    this.wait();
                }
            }
        } finally {
            closeReceive();
            System.out.println("end");
        }
    }

    public static void main(String[] args) throws Exception {
        String queueName = "jms/Queue2";
        Broker broker = new Broker();
        broker.receive(queueName);
    }
}