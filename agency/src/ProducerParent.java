import java.util.Hashtable;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ProducerParent {

    private QueueConnection connection;
    private QueueSession session;
    private QueueSender sender;

    private TextMessage msg;

    private void init(Context ctx, String queueName) throws NamingException, JMSException {
        QueueConnectionFactory factory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        connection = factory.createQueueConnection();

        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue queue = (Queue) ctx.lookup(queueName);

        sender = session.createSender(queue);
        msg = session.createTextMessage();
    }

    private void close() throws JMSException {
        sender.close();
        session.close();
        connection.close();
    }

    private void send(String queueName, String message) throws Exception {
        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ic = new InitialContext(env);
        init(ic, queueName);
        try {
            msg.setText(message);
            sender.send(msg, DeliveryMode.PERSISTENT, 8, 0);
        } finally {
            close();
        }
    }

    public static void main(String[] args) throws Exception {
        String queueName = "jms/Queue2";
        ProducerParent client = new ProducerParent();

        client.send(queueName, "boook 1");
        client.send(queueName, "vylet 1");
        client.send(queueName, "book 2");
        client.send(queueName, "vylet 2");
    }
}