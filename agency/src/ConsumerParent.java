import java.util.Hashtable;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ConsumerParent implements MessageListener {

    private QueueConnection connection;
    private QueueSession session;
    private QueueReceiver receiver;
    private Queue queue;

    private void initReceive(Context ctx, String queueName) throws NamingException, JMSException {
        QueueConnectionFactory qconFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        connection = qconFactory.createQueueConnection();

        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        queue = (Queue) ctx.lookup(queueName);

        receiver = session.createReceiver(queue);
        receiver.setMessageListener(this);
        connection.start();
    }

    private void closeReceive() throws JMSException {
        receiver.close();
        session.close();
        connection.close();
    }

    public void onMessage(Message msg) {
        try {
            String msgText;
            if (msg instanceof TextMessage) {
                msgText = ((TextMessage) msg).getText();
            } else {
                msgText = msg.toString();
            }

            System.out.println("recieve: \"" + msgText + "\"");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void receive(String queueName) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ic = new InitialContext(env);
        initReceive(ic, queueName);

        System.out.println("recieve: " + queue.toString());

        try {
            synchronized (this) {
                while (true) {
                    this.wait();
                }
            }
        } finally {
            closeReceive();
            System.out.println("end");
        }
    }
}