package message;

import java.util.ArrayList;

public class Message {

    private String name;
    private String value;
    private ArrayList<Message> values;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ArrayList<Message> getValues() {
        return values;
    }

    public void setValues(ArrayList<Message> values) {
        this.values = values;
    }

    public void reformatArray(){
        Message messageName = new Message();
        Message messageSurname = new Message();
        String[] split = value.split(" ");

        messageName.name = "name";
        messageName.value = split[0];

        messageSurname.name = "surname";
        messageSurname.value = split[1];


        values = new ArrayList<>();
        values.add(messageName);
        values.add(messageSurname);
    }
}

