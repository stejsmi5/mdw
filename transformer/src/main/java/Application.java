import loader.DataLoader;
import messageFormat.MessageTransformer;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;

public class Application {

    private final static Logger logger = Logger.getLogger(Application.class);

    public static void main(String args[]) {
        MessageTransformer format;

        try {
            DataLoader loader = DataLoader.getLoader("/Users/admin/Desktop/input.txt");
            format = loader.getMessage();
            format.reformat();

        } catch (IOException e) {
            logger.error("Cannot load data");
            return;
        }
    }
}
