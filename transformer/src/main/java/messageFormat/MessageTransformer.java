package messageFormat;

import message.Message;

import java.util.ArrayList;
import java.util.List;


public class MessageTransformer {

    private List<String> messages;

    public MessageTransformer() {
        this.messages = new ArrayList<>();
    }

    public void addMessage(String message){
        this.messages.add(message);
    }

    public void reformat(){
        List<Message> messageList = new ArrayList<>();

        messages.forEach( messageString -> {
            if(messageString.length() > 0){
                String[] split = messageString.split(":");
                if(split.length >= 2){

                    Message message = new Message();
                    message.setName(split[0].substring(!split[0].contains(" ") ? 0 : split[0].indexOf(" ") + 1, split[0].length()).replaceAll("\"", "").toLowerCase());
                    message.setValue(split[1].startsWith(" ") ? split[1].substring(1, split[1].length()).replaceAll("\"", "") : split[1].replaceAll("\"", ""));

                    if(message.getName().equalsIgnoreCase("person")){
                        message.reformatArray();
                    }
                    messageList.add(message);
                }
            }
        });

        printReformattedMessage(messageList);
    }

    private void printReformattedMessage(List<Message> messageList){
        int i = 0;
        System.out.println("{");

        for(Message message: messageList){
            if(message.getValues() == null || message.getValues().isEmpty()) {
                System.out.println("  \"" + message.getName() + "\": \"" + message.getValue() + "\"" + (i == (messageList.size()-1) ? "" : ","));
            }else{
                int j = 0;
                System.out.println("  \"" + message.getName() + "\": {" );
                for(Message subMessage: message.getValues()) {
                    System.out.println( "    \"" + subMessage.getName() + "\": \"" + subMessage.getValue() + "\"" + (j == (message.getValues().size()-1) ? "" : ","));
                    j++;
                }
                System.out.println( "  }" + (i == (messageList.size()-1) ? "" : ",") );
            }
            i++;
        }
        System.out.println("}");
    }
}
