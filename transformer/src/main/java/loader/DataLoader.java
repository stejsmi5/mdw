package loader;

import messageFormat.MessageTransformer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class DataLoader {

    private static DataLoader loader;
    private String fileName;

    private DataLoader(String fileName) {
        this.fileName = fileName;
    }

    public static DataLoader getLoader(String fileName) {
        if (loader == null) {
            loader = new DataLoader(fileName);
        }
        return loader;
    }

    public MessageTransformer getMessage() throws IOException {
        return load();
    }


    private MessageTransformer load() throws IOException {
        File file = new File(this.fileName);
        FileReader fileReader;
        fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        return readFile(bufferedReader);
    }

    private MessageTransformer readFile(BufferedReader bufferedReader) throws IOException {
        MessageTransformer format = new MessageTransformer();
        String line;
        boolean read = false;


        while ((line = bufferedReader.readLine()) != null) {
            if (line.startsWith("===")) {
                read = !read;
                continue;
            }

            if (read) {
                format.addMessage(line);
            }
        }

        return format;
    }
}
