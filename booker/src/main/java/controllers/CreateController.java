package controllers;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/book")
public class CreateController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Cookie[] cookies = request.getCookies();
        String sessionId = findSessionId(cookies);

        if(sessionId == null){
            response.addCookie(new Cookie("JSESSIONID", request.getSession().getId()));
            request.getSession().setAttribute("state", "NEW");
            SessionHandler.sessions.put(request.getSession().getId(), request.getSession());
            response.getWriter().print("NEW\n" + request.getSession().getId() + "\n");
        }else{
            HttpSession session = SessionHandler.sessions.get(sessionId);
            if(session == null){
                response.getWriter().print("INVALID\n");
                return;
            }
            Object state = session.getAttribute("state");
            if(state.equals("NEW")){
                request.getSession().setAttribute("state", "WAITING FOR PAYMENT");
                response.getWriter().print("WAITING FOR PAYMENT\n" + sessionId + "\n");
            }else if(state.equals("WAITING FOR PAYMENT")){
                SessionHandler.sessions.remove(sessionId);
                response.getWriter().print("COMPLETED\n" + sessionId + "\n");
            }
        }

    }

    private String findSessionId(Cookie[] cookies){


        if(cookies == null) return null;
        for(Cookie cookie: cookies){
            if(cookie != null && cookie.getName().equals("JSESSIONID")){
                return cookie.getValue();
            }
        }

        return null;
    }
}
