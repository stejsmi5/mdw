package mdw;

import mdw.service.TripManagerImplementation;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.ws.Endpoint;

@SpringBootApplication
public class SoapApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoapApplication.class, args);
        Endpoint.publish("http://localhost:9999/ws/trip", new TripManagerImplementation());

    }
}
