package mdw.service;

import mdw.model.Trip;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use= SOAPBinding.Use.LITERAL)
public interface TripManager {

    @WebMethod
    List<Trip> getAll();

    @WebMethod
    void addTrip(Trip trip);

    @WebMethod
    void removeTrip(Trip trip);

}
