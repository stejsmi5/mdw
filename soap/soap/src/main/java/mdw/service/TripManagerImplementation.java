package mdw.service;

import mdw.model.Trip;
import mdw.repository.TripRepository;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "mdw.service.TripManager")
public class TripManagerImplementation implements TripManager{


    @Override
    public List<Trip> getAll() {
        return TripRepository.getAll();
    }

    @Override
    public void addTrip(Trip trip) {
        TripRepository.add(trip);
    }

    @Override
    public void removeTrip(Trip trip) {
        TripRepository.remove(trip);
    }
}
