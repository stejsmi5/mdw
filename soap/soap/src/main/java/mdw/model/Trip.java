package mdw.model;


import java.util.List;

public class Trip {
    private String name;
    private String from;
    private String to;
    private List<String> passengers;

    public Trip(){}

    public Trip(String name, String from, String to, List<String> passengers) {
        this.name = name;
        this.from = from;
        this.to = to;
        this.passengers = passengers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public List<String> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<String> passengers) {
        this.passengers = passengers;
    }
}
