package mdw.repository;

import mdw.model.Trip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TripRepository {

    private static TripRepository ourInstance = new TripRepository();
    private static List<Trip> tripList;

    public static TripRepository getInstance() {
        return ourInstance;
    }

    private TripRepository() {
        tripList = new ArrayList<>();
        init();
    }

    public static void add(Trip trip){
        tripList.add(trip);
    }

    public static void remove(Trip trip){
        remove(trip.getName());
    }

    public static void remove(String name){
        for(int i=0; i<tripList.size(); i++){
            if(name.equals(tripList.get(i).getName())){
                tripList.remove(tripList.get(i));
            }
        }
    }

    public static List<Trip> getAll(){
        return tripList;
    }

    private void init(){
        Trip t1 = new Trip("usa", "cz", "usa", Arrays.asList("Josef", "Jan", "Martin"));
        Trip t2 = new Trip("uk", "cz", "uk", Arrays.asList("Martin", "Jana", "Michal"));
        Trip t3 = new Trip("ru", "cz", "ru", Arrays.asList("František", "Lucie", "Eva"));

        tripList.add(t1);
        tripList.add(t2);
        tripList.add(t3);
    }
}
