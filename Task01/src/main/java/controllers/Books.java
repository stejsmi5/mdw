package main.java.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.models.DB;
import main.java.models.DBPerson;
import main.java.models.Person;
import main.java.models.Trip;
import weblogic.servlet.annotation.WLServlet;

@WLServlet ( name = "books", mapping = {"/books"})
public class Books extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Person> books = DBPerson.getInstance().getAll();	
		
		response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>Vsechny booky</h1>");
       
        if(books != null){
	        for(Person trip: books){
	        	out.println( trip.getName() + "\t" + trip.getTrip().getName() );
	        	out.println("<br>");
	        }
        }
        
        out.println("</body>");
        out.println("</html>");
	}
}
