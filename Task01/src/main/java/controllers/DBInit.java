package main.java.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.models.DB;
import main.java.models.Trip;
import weblogic.servlet.annotation.WLServlet;

/**
 * Servlet implementation class DBInit
 */

@WLServlet ( name = "ExamplePrintServlet22", mapping = {"/DBInit"})
public class DBInit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DBInit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DB.getInstance().addObject(new Trip(1, "aa", 11));
		DB.getInstance().addObject(new Trip(2, "ab", 11));
		DB.getInstance().addObject(new Trip(3, "ac", 11));
		DB.getInstance().addObject(new Trip(4, "ad", 11));
		
		
		response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>INIT </h1>");
        
        out.println("</body>");
        out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
