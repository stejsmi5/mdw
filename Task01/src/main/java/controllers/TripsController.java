package main.java.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.models.DB;
import main.java.models.Trip;
import weblogic.servlet.annotation.WLServlet;


@WLServlet ( name = "ExamplePrintServlet", mapping = {"/trips"})
public class TripsController extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
		List<Trip> trips = DB.getInstance().getAll();
		
		response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>Vsechny </h1>");
        
        
        for(Trip trip: trips){
        	out.println("<form action=\"book\" method=\"GET\">");
        	out.println(trip.getId() + "\t" + trip.getName() + "\t" + trip.getCapacity() + "\t" + trip.getOccupied());
        	out.println("<input type=\"hidden\" name=\"tripId\" value=\"" + trip.getId() + "\" >");
        	out.println("<input name=\"personName\">" + "<input type=\"submit\"> <br>");
        	out.println("</form>");
        }
        
        
        out.println("</body>");
        out.println("</html>");
    }
}
