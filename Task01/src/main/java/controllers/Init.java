package main.java.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.models.DB;
import main.java.models.Trip;
import weblogic.servlet.annotation.WLServlet;

@WLServlet ( name = "ExamplePrintServlet", mapping = {"/init"})
public class Init extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DB.getInstance().addObject(new Trip(1, "aa", 11));
		DB.getInstance().addObject(new Trip(2, "ab", 11));
		DB.getInstance().addObject(new Trip(3, "ac", 11));
		DB.getInstance().addObject(new Trip(4, "ad", 11));
		
		
		response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>INIT </h1>");
        
        out.println("</body>");
        out.println("</html>");
	}

	

}
