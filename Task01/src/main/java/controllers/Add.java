package main.java.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.models.DB;
import main.java.models.Trip;
import weblogic.servlet.annotation.WLServlet;

@WLServlet ( name = "add", mapping = {"/add"})
public class Add extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
   		String cap = request.getParameter("capacity");
   		
   		DB.getInstance().addObject(new Trip(11, name, Integer.parseInt(cap)));
		
   		response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>PRIDANO </h1>");
        	
        out.println("</body>");
        out.println("</html>");
	}

}
