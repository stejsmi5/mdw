package main.java.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.models.DB;
import main.java.models.Trip;
import weblogic.servlet.annotation.WLServlet;

@WLServlet ( name = "addTrip", mapping = {"/addTrip"})
public class AddTrip extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   		
   		response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>Add </h1>");
        	
        out.println("<form action=\"add\" method=\"GET\">");
        out.println("<input name=\"name\">");
        out.println("<input name=\"capacity\">");
        out.println("<input type=\"Submit\">");
        out.println("</form>");
    
        		
        out.println("</body>");
        out.println("</html>");
		response.getWriter().append("Served at: ").append(request.getContextPath());
   	}
}
