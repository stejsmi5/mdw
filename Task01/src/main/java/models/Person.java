package main.java.models;

public class Person {
	private String name;
	private Trip trip;
	
	public Person(String name, Trip trip) {
		super();
		this.name = name;
		this.trip = trip;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Trip getTrip() {
		return trip;
	}
	public void setTrip(Trip trip) {
		this.trip = trip;
	}
}
