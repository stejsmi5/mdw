package main.java.models;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
 
public class DB {
    private static DB instance = null;
    private Hashtable<Integer, Trip> ht = new Hashtable();
 
    public static DB getInstance() {
        if (instance == null)
            instance = new DB();
        return instance;
    }
    public void addObject(Trip t){
        ht.put(t.getId(), t);
    }
    public Trip getObject(int id) {
        return ht.get(id);
    }
    
    public boolean book(int id){
    	return ht.get(id).book();
    }
    
    @SuppressWarnings("unchecked")
	public List<Trip> getAll(){
    	List<Trip> trips = new ArrayList<>();
    	
    	Enumeration k = ht.keys();
    	while(k.hasMoreElements()){
    		trips.add(ht.get(k.nextElement()));
    	}
    	
    	return trips;
    	
    }
}
