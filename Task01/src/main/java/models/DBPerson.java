package main.java.models;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
 
public class DBPerson {
    private static DBPerson instance = null;
    private Hashtable<String, Person> ht = new Hashtable();
 
    public static DBPerson getInstance() {
        if (instance == null)
            instance = new DBPerson();
        return instance;
    }
    public void addObject(Person t){
        ht.put(t.getName(), t);
    }
    
    public Person getObject(String id) {
        return ht.get(id);
    }
       
    @SuppressWarnings("unchecked")
	public List<Person> getAll(){
    	List<Person> people = new ArrayList<>();
    	
    	Enumeration k = ht.keys();
    	while(k.hasMoreElements()){
    		people.add(ht.get(k.nextElement()));
    	}
    	
    	return people;
    	
    }
}
