package main.java.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import weblogic.servlet.annotation.WLFilter;

@WLFilter (name = "addFilter", mapping = {"/add"})
public class AddFilter implements Filter {
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		long start = System.currentTimeMillis();
        // pass the request along the filter chain
        chain.doFilter(request, response);
        System.out.println("ExampleLogFilter: "+((HttpServletRequest)request).getRequestURI() + "\t add trip: " + ((HttpServletRequest)request).getParameter("name") + " - "+String.valueOf(System.currentTimeMillis()-start)+" ms");
	}

	
	public void init(FilterConfig fConfig) throws ServletException {}public AddFilter() {}
	public void destroy() {}

}
