<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
    <title>allTrips</title>
</head>
<body>

<a href="/trips"> trips </a>
<hr>

<h1>add trips</h1>

<form:form method="POST" action="${formUrl}" modelAttribute="trip">
    <table>
        <tr>
            <td><form:label path="id">id</form:label></td>
            <td><form:input path="id"/></td>            
        </tr>

        <tr>
            <td><form:label path="name">name</form:label></td>
            <td><form:input path="name"/></td>            
        </tr>

        <tr>
            <td><form:label path="capacity">capacity</form:label></td>
            <td><form:input path="capacity"/></td>
        </tr>

        <tr>
            <td><input type="submit" value="Submit"/></td>
        </tr>
    </table>
</form:form>


<h1>all trips</h1>
<c:forEach var="trip" items="${trips}">
    ${trip.id} ${trip.name} ${trip.capacity} ${trip.occupied}<br>
</c:forEach>

</body>
</html>
