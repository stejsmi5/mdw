package cz.mdw.model;

import java.io.Serializable;

public class Trip implements Serializable {
    int id;
    private String name;
    private int capacity;
    private int occupied;

    public Trip(){
        this.occupied = 0;
    }

    public Trip(int id, String name, int capacity){
        this.id = id;
        this.name = name;
        this.capacity = capacity;
    }


    public boolean book(){

        if(this.occupied < this.capacity){
            this.occupied++;
            return true;
        }
        return false;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getCapacity() {
        return capacity;
    }
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
    public int getOccupied() {
        return occupied;
    }
    public void setOccupied(int occupied) {
        this.occupied = occupied;
    }
}
