package cz.mdw.model;


import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ModelInterface extends Remote {

    public List<Trip> getAllTrips() throws RemoteException;
    public boolean addNewTrip(Trip trip) throws RemoteException;

}
