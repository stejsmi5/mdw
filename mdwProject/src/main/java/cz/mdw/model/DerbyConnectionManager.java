package cz.mdw.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DerbyConnectionManager {
    private static Connection connection = null;

    public static Connection getConnection(){
        if(connection == null){
            try {

                String driver = "org.apache.derby.jdbc.EmbeddedDriver";
                String connectionURL = "jdbc:derby:memory:testDB;create=true";
                Class.forName(driver);
                connection = DriverManager.getConnection(connectionURL);

                createTable();
                insertInitData();

            } catch (ClassNotFoundException e) {
                System.err.println("ERROR WHILE GETTING CONNECTION!!" + e);
            } catch (SQLException e) {
                System.err.println("ERROR WHILE GETTING CONNECTION!!" + e);
            }

        }

        return connection;
    }

    private static boolean createTable(){
        String DDL = "CREATE TABLE trips (" +
                    "id INT NOT NULL, " +
                    "name VARCHAR(20) NOT NULL, " +
                    "capacity INT, " +
                    "PRIMARY KEY (id))";

        return executeUpdateStatement(DDL) != 0;
    }

    private static void insertInitData(){
        String DML = "INSERT INTO trips VALUES (1, 'USA', 10), (2, 'CZ', 10), (3, 'UK', 10)";
        executeUpdateStatement(DML);
    }

    public static List<Trip> getAll() {
        List<Trip> trips = new ArrayList<Trip>();
        try {
            String DML = "SELECT * FROM trips";
            ResultSet resultSet = executeStatement(DML);

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int capacity = resultSet.getInt("capacity");

                trips.add(new Trip(id, name, capacity));
                System.out.println(id + " " + name + " " + capacity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return trips;
    }

    public static boolean addTrip(Trip trip){
        String DML = "INSERT INTO trips VALUES ("+ trip.getId() + ", '" + trip.getName()+"',"+ trip.getCapacity() +")";
        return executeUpdateStatement(DML) != 0? true : false;
    }

    private static ResultSet executeStatement(String DML){
        Statement stmnt = null;
        ResultSet rs = null;
        try {

            stmnt = getConnection().createStatement();
            rs = stmnt.executeQuery(DML);
        } catch (SQLException e) {
            System.err.println("ERROR WHILE EXECUTING QUERY!!" + e);
        }

        return rs;
    }

    private static int executeUpdateStatement(String DML){
        Statement stmnt = null;
        try {

            stmnt = getConnection().createStatement();
            return stmnt.executeUpdate(DML);
        } catch (SQLException e) {
            System.err.println("ERROR WHILE EXECUTING QUERY!!" + e);
        }
        return 0;
    }
}
