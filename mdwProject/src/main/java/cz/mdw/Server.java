package cz.mdw;




import cz.mdw.model.DerbyConnectionManager;
import cz.mdw.model.ModelInterface;
import cz.mdw.model.Trip;

import java.net.URL;
import java.net.URLClassLoader;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class Server extends UnicastRemoteObject implements ModelInterface {

    public Server() throws RemoteException {
        super();
    }

    public List<Trip> getAllTrips() throws RemoteException {
        return DerbyConnectionManager.getAll();
    }

    public boolean addNewTrip(Trip trip) throws RemoteException {
        return DerbyConnectionManager.addTrip(trip);
    }

    public static void main(String[] args) {

        showClassPath();

        try {
            ModelInterface inMemoryDatabase = new Server();
            Naming.rebind("db", inMemoryDatabase);
            System.err.println("rmi.Server ready");
        } catch (Exception e) {
            System.err.println("rmi.Server exception");
            e.printStackTrace();
        }
    }

    private static void showClassPath(){
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        URL[] urls = ((URLClassLoader)cl).getURLs();
        for(URL url: urls){
            System.out.println(url.getFile());
        }
    }
}
