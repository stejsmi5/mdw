package cz.mdw.controller;

import cz.mdw.model.ModelInterface;
import cz.mdw.model.Trip;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.rmi.Naming;
import java.util.Collections;
import java.util.List;

@Controller
public class TripController {

    @RequestMapping(value = "/trips", method = RequestMethod.GET)
    public ModelAndView getAll() {

        List<Trip> trips = getAllTrips();
        ModelAndView model = new ModelAndView();
        model.setViewName("trips");
        model.addObject("trips", trips);
        model.addObject("trip", new Trip());

        return model;
    }

    @RequestMapping(value = "/trips", method = RequestMethod.POST)
    public ModelAndView showAddUserForm(@ModelAttribute("trip") Trip trip) {
        if(trip != null) {
            addTrip(trip);
        }

        return getAll();
    }



    private List<Trip> getAllTrips() {
        List<Trip> trips = null;

        try {
            ModelInterface db = (ModelInterface) Naming.lookup("rmi://127.0.0.1/db");
            trips = db.getAllTrips();
            return trips;

        } catch (Exception e) {
            System.err.println("rmi.Client exception: " + e.toString());
        }

        return Collections.emptyList();
    }

    private boolean addTrip(Trip trip){
        try {
            ModelInterface db = (ModelInterface) Naming.lookup("rmi://127.0.0.1/db");
            return  db.addNewTrip(trip);

        } catch (Exception e) {
            System.err.println("rmi.Client exception: " + e.toString());
        }

        return false;
    }
}
